// ## app.js ##
//
// This is where we set up our web application using Express. We create the
// app and set up routes which will respond to requests from the client's
// web browser.

const path = require('path');
const express = require('express');
import styles from './public/styles.css'
// Create a new Express app
const app = express();

// Serve up our static assets from 'dist' (this includes our client-side
// bundle of JavaScript). These assets are referred to in the HTML using
// <link> and <script> tags.
app.use(express.static('public'));

// Set up the index route
app.get('/', (req, res) => {
    // The HTML is pretty barebones, it just provides a mount point
    // for React and links to our styles and scripts.
    res.sendFile(__dirname + '/public/index.html');
});
app.use(function(req, res, next) {
    res.redirect('/');
    next();
})

// Export the Express app
module.exports = app;